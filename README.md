# Argus

![Anvil logo](https://www.drupal.org/files/styles/grid-4-2x/public/Anvil_Logo_full_cropped_padded.jpg)

## Contents of this file

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

Module that exposes a set of additional Sensors, to track various statuses of a website concerning its health and/or safety.

Some new Sensors include :

- Admin Username : monitors the administrator username (e.g. UID 1) for some common, unsafe usernames
- Aggregation : monitors the CSS and JS aggregation settings
- Core version : monitors the installed Drupal Core version
- Error reporting : monitors the error level setting
- Memory Limit : monitors the website's configured memory limit
- Page Cache : monitors the installation of the Core Page Cache modules
- Page Cache max age : monitors the Page Cache max age setting
- PHP version : monitors the PHP version
- Module & Theme release : monitors the current installed release of modules & themes
- Site email : monitors the site's mail address for specific domains

In addition, some REST endpoints are exposed for Slack interation.

## Requirements

- Requires [Monitoring](https://www.drupal.org/project/monitoring)
- Requires PHP 8.0 or greater

## Installation

1. Add the Anvil repository to the `repositories` section of your `composer.json`

```
{
    "type": "vcs",
    "url": "git@gitlab.com:anvil-pub/anvil_argus.git"
}
```

2. Add `anvil-drupal-module` as a new installer type to the `extra` section of your `composer.json`

```
"installer-types": [
    "anvil-drupal-module"
],
```

3. Define an installer path for this new installer type in the `extra` section of your `composer.json`

```
"web/modules/anvil/{$name}": [
    "type:anvil-drupal-module"
]
```

4. Add `/web/modules/anvil/` to `.gitignore` to exclude the new installer path


5. Require the Anvil Argus module using composer. The exact version depending on the installed Drupal and PHP:

|                         | Support            | Command                                                               |
|-------------------------|--------------------|-----------------------------------------------------------------------|
| Latest `rc`             | Drupal 8+ & PHP 8+ | `composer require anvil/anvil_argus:^1.0@rc'`      |
| Latest `8.x-1.x`         | Drupal 8+ & PHP 7+ | `composer require anvil/anvil_argus:dev-8.x-1.x`       |

6. If done correctly, the Anvil Argus modules should now be located at `web/modules/anvil/anvil_argus`


7. Install as you would normally install a contributed Drupal module. See [installing modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Configuration

Upon module installation, all Argus-specific Sensors will be automatically enabled, including a Sensor for each and every
installed contrib module and theme.
Whenever a new module or theme is (un)installed, a corresponding Sensor will be created or deleted.

By default, various Monitoring Sensors will be enabled as well. If you do not want this, go to `admin/config/system/monitoring/sensors`
and disable any Sensors you do not need.  
All Argus-specific Sensors are located under the 'Argus' category.

The quickest way to do this:
* first uncheck all Sensors using the very top checkbox at the overview table
* execute the following command in your browser's console: `jQuery('input[name^="sensors[argus"]').prop('checked', 'checked');`

If the website containing the Argus Client is meant to be monitored, go to `admin/config/system/argus-settings` and make
sure there is a secret key generated.  
If there isn't - or you want a new secret key - simply click the 'Regenerate secret key' button or use the `argus:generate-secret` Drush command.

## Maintainers

[Anvil.be](https://www.anvil.be)

