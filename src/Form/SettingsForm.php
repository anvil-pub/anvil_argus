<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Form;

use Drupal\anvil_argus\ArgusSecretManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends FormBase {

  /**
   * The Argus secret manager.
   *
   * @var \Drupal\anvil_argus\ArgusSecretManager
   */
  protected ArgusSecretManager $argusSecretManager;

  public function __construct(ArgusSecretManager $argus_secret_manager) {
    $this->argusSecretManager = $argus_secret_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('anvil_argus.secret_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'anvil_argus_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['argus_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $this->argusSecretManager->getSecretKey(),
      '#description' => [
        ['#markup' => $this->t('The secret key the server needs, to access this site\'s Sensor data.')],
        ['#markup' => '<br />'],
        ['#markup' => $this->t('This key should be passed using the %secret_key header or query string.', [
          '%secret_key' => ArgusSecretManager::SECRET_KEY_NAME
        ])],
      ],
      '#attributes' => [
        'disabled' => 'disabled',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Regenerate secret key'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->argusSecretManager->generateSecretKey();
  }

}