<?php

declare(strict_types=1);

namespace Drupal\anvil_argus;

use Drupal\anvil_argus\Result\ArgusSensorResultInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\monitoring\Result\SensorResultInterface;

class ArgusResult {

  /**
   * @var string
   */
  protected string $id;

  /**
   * @var mixed
   */
  protected mixed $value;

  /**
   * @var string
   */
  protected string $status;

  /**
   * @var string|\Drupal\Component\Render\FormattableMarkup
   */
  protected string|FormattableMarkup $message;

  /**
   * The timestamp when the result was fetched.
   *
   * @var int
   */
  protected int $timestamp;

  /**
   * The time it took for the result to be executed.
   *
   * @var float
   */
  protected float $executionTime;

  protected array $metaData = [];

  /**
   * @var string
   */
  protected string $subcategory = 'general';

  /**
   * @param string $id
   * @param \Drupal\monitoring\Result\SensorResultInterface|\Drupal\anvil_argus\Result\ArgusSensorResultInterface $sensor_result
   */
  public function __construct(string $id, SensorResultInterface|ArgusSensorResultInterface $sensor_result) {
    $this->setId($id);

    $this
      ->setValue($sensor_result->getValue())
      ->setStatus($sensor_result->getStatus())
      ->setMessage($sensor_result->getMessage())
      ->setTimestamp($sensor_result->getTimestamp())
      ->setExecutionTime($sensor_result->getExecutionTime())
      ->setMetaData($sensor_result->getMetaData());

    // Set the result's 'subcategory', if available.
    if ($sensor_config_subcategory = $sensor_result->getSensorConfig()->getSetting('subcategory')) {
      $this->setSubcategory($sensor_config_subcategory);
    }
  }

  /**
   * @param bool $short_key
   *   Whether to return the ID, stripped of any prefixes.
   *
   * @return string
   */
  public function getId(bool $short_key = FALSE): string {
    return ($short_key) ? str_replace(['argus_', 'module_release_', 'theme_release_'], '', $this->id) : $this->id;
  }

  /**
   * @param string $id
   *
   * @return $this
   */
  public function setId(string $id): ArgusResult {
    $this->id = $id;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getValue(): mixed {
    return $this->value;
  }

  /**
   * @param mixed $value
   *
   * @return $this
   */
  public function setValue(mixed $value): ArgusResult {
    $this->value = $value;
    return $this;
  }

  /**
   * @return string
   */
  public function getStatus(): string {
    return $this->status;
  }

  /**
   * @param string $status
   *
   * @return $this
   */
  public function setStatus(string $status): ArgusResult {
    $this->status = $status;
    return $this;
  }

  /**
   * @return string|\Drupal\Component\Render\FormattableMarkup
   */
  public function getMessage(): string|FormattableMarkup {
    return $this->message;
  }

  /**
   * @param string|\Drupal\Component\Render\FormattableMarkup $message
   *
   * @return $this
   */
  public function setMessage(string|FormattableMarkup $message): ArgusResult {
    $this->message = $message;
    return $this;
  }

  /**
   * @return int
   */
  public function getTimestamp(): int {
    return $this->timestamp;
  }

  /**
   * @param int $timestamp
   *
   * @return $this
   */
  public function setTimestamp(int $timestamp): ArgusResult {
    $this->timestamp = $timestamp;
    return $this;
  }

  /**
   * @return float
   */
  public function getExecutionTime(): float {
    return $this->executionTime;
  }

  /**
   * @param float $executionTime
   *
   * @return $this
   */
  public function setExecutionTime(float $executionTime): ArgusResult {
    $this->executionTime = $executionTime;
    return $this;
  }

  /**
   * @return string
   */
  public function getSubcategory(): string {
    return $this->subcategory;
  }

  /**
   * @param string $subcategory
   *
   * @return $this
   */
  public function setSubcategory(string $subcategory): ArgusResult {
    $this->subcategory = $subcategory;
    return $this;
  }

  public function getMetaData(): ?array {
    return $this->metaData ?? NULL;
  }

  public function setMetaData(array $data) {
    $this->metaData = $data;
    return $this;
  }

  /**
   * @return array
   */
  public function toArray(): array {
    $data = [
      'value' => $this->getValue(),
      'status' => $this->getStatus(),
      'message' => $this->getMessage(),
      'timestamp' => $this->getTimestamp(),
      'execution_time' => $this->getExecutionTime(),
    ];

    // ..
    $meta_data = $this->getMetaData();
    if (!empty($meta_data)) {
      $data['meta_data'] = $meta_data;
    }

    return $data;
  }

}