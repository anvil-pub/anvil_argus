<?php

declare(strict_types=1);

namespace Drupal\anvil_argus;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;

class ArgusExtensionManager {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected ThemeExtensionList $themeExtensionList;

  /**
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   * @param \Drupal\Core\Extension\ThemeExtensionList $theme_extension_list
   */
  public function __construct(ModuleExtensionList $module_extension_list, ThemeExtensionList $theme_extension_list) {
    $this->moduleExtensionList = $module_extension_list;
    $this->themeExtensionList = $theme_extension_list;
  }

  /**
   * Fetches modules, optionally filtered.
   *
   * @param bool $include_core
   *   (optional) Whether to include Core extensions. Defaults to FALSE.
   * @param bool $include_subextensions
   *   (optional) Whether to include subextensions. Defaults to FALSE.
   * @param bool $include_disabled
   *   (optional) Whether to include disabled extensions. Defaults to FALSE.
   *
   * @return Extension[]
   *   Array of module-type extensions.
   */
  public function getModules(bool $include_core = FALSE, bool $include_subextensions = FALSE, bool $include_disabled = FALSE): array {
    $extensions = $this->moduleExtensionList->getList();

    return $this->filterExtensions($extensions, $include_core, $include_subextensions, $include_disabled);
  }

  /**
   * Fetches themes, optionally filtered.
   *
   * @param bool $include_core
   *   (optional) Whether to include Core extensions. Defaults to FALSE.
   * @param bool $include_subextensions
   *   (optional) Whether to include subextensions. Defaults to FALSE.
   * @param bool $include_disabled
   *   (optional) Whether to include disabled extensions. Defaults to FALSE.
   *
   * @return Extension[]
   *   Array of theme-type extensions.
   */
  public function getThemes(bool $include_core = FALSE, bool $include_subextensions = FALSE, bool $include_disabled = FALSE): array {
    $extensions = $this->themeExtensionList->getList();

    return $this->filterExtensions($extensions, $include_core, $include_subextensions, $include_disabled);
  }

  /**
   * Loads modules.
   *
   * @param array $modules
   *   List of modules to load the extensions for.
   *
   * @return Extension[]
   *   Array of module-type extensions.
   */
  public function loadModules(array $modules): array {
    $extensions = [];

    foreach ($modules as $module) {
      if ($extension = $this->moduleExtensionList->get($module)) {
        $extensions[$module] = $extension;
      }
    }

    return $extensions;
  }

  /**
   * Loads themes.
   *
   * @param array $themes
   *   List of themes to load the extensions for.
   *
   * @return Extension[]
   *   Array of theme-type extensions.
   */
  public function loadThemes(array $themes): array {
    $extensions = [];

    foreach ($themes as $theme) {
      if ($extension = $this->themeExtensionList->get($theme)) {
        $extensions[$theme] = $extension;
      }
    }

    return $extensions;
  }

  /**
   * Filters an array of extensions.
   *
   * @param bool $include_core
   *   Whether to include Core extensions.
   * @param bool $include_subextensions
   *   Whether to include subextensions.
   * @param bool $include_disabled
   *   Whether to include disabled extensions.
   *
   * @return Extension[]
   *   The filtered array of Extensions
   */
  private function filterExtensions(array $extensions, bool $include_core, bool $include_subextensions, bool $include_disabled): array {
    foreach ($extensions as $extension_key => $extension) {
      // Filter out Core extensions.
      if (!$include_core && property_exists($extension, 'origin') && $extension->origin === 'core') {
        unset($extensions[$extension_key]);
        continue;
      }

      // Filter out subextensions ( e.g. Devel Generate ).
      if (!$include_subextensions && isset($extension->info['project']) && $extension->info['project'] !== $extension_key) {
        unset($extensions[$extension_key]);
        continue;
      }

      // Filter out disabled extensions.
      if (!$include_disabled && $extension->status === 0) {
        unset($extensions[$extension_key]);
      }

      // Filter out custom extensions. This happens by default.
      if (property_exists($extension, 'subpath') && str_contains($extension->subpath, 'custom/')) {
        unset($extensions[$extension_key]);
      }

      // Lastly, filter out profiles by default.
      if ($extension->getType() === 'profile') {
        unset($extensions[$extension_key]);
      }
    }

    ksort($extensions);

    return $extensions;
  }

}