<?php

declare(strict_types=1);

namespace Drupal\anvil_argus;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Sensor\SensorManager;
use Drupal\monitoring\SensorRunner;
use Drupal\anvil_argus\Result\ArgusSensorResult;

class ArgusSensorRunner extends SensorRunner {

  protected SensorRunner $sensorRunner;

  public function __construct(SensorRunner $sensor_runner, SensorManager $sensor_manager, CacheBackendInterface $cache, ConfigFactoryInterface $config_factory) {
    $this->sensorRunner = $sensor_runner;

    parent::__construct($sensor_manager, $cache, $config_factory);
  }

  /**
   * {@inheritdoc}
   */
  protected function getResultObject(SensorConfig $sensor_config): ArgusSensorResult {
    $result_class = '\Drupal\anvil_argus\Result\ArgusSensorResult';

    if (!$this->forceRun && isset($this->sensorResultCache[$sensor_config->id()])) {
      $result = new $result_class($sensor_config, $this->sensorResultCache[$sensor_config->id()]);
    }
    else {
      $result = new $result_class($sensor_config);
    }
    return $result;
  }

}