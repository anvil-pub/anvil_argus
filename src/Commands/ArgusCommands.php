<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Commands;

use Drupal\anvil_argus\ArgusSecretManager;
use Drush\Commands\DrushCommands;

class ArgusCommands extends DrushCommands {

  /**
   * The Argus secret manager.
   *
   * @var \Drupal\anvil_argus\ArgusSecretManager
   */
  protected ArgusSecretManager $argusSecretManager;

  /**
   * Constructor.
   *
   * @param \Drupal\anvil_argus\ArgusSecretManager $argus_secret_manager
   */
  public function __construct(ArgusSecretManager $argus_secret_manager) {
    parent::__construct();

    $this->argusSecretManager = $argus_secret_manager;
  }

  /**
   * Generates a secret key and saves it into the State API.
   *
   * @command argus:generate-secret
   *
   * @option length The desired length of the secret key
   */
  public function generateSecretKey(array $options = ['length' => ArgusSecretManager::SECRET_MIN_LENGTH]): void {
    if ($secret_key = $this->argusSecretManager->generateSecretKey(TRUE, $options['length'])) {
      $this->output()->writeln($secret_key);
    }
  }

}