<?php

declare(strict_types=1);

namespace Drupal\anvil_argus;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\State\StateInterface;

class ArgusSecretManager {

  public const SECRET_KEY_NAME = 'argus-client';

  public const SECRET_MIN_LENGTH = 32;

  /**
   * The state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * @param \Drupal\Core\State\StateInterface $state
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * Generates a secret key.
   *
   * @param bool $save_secret
   *   (optional) Whether to save the generated secret into the State API.
   * @param int $secret_length
   *   (optional) The desired length of the secret key.
   *
   * @return string
   */
  public function generateSecretKey(bool $save_secret = TRUE, int $secret_length = self::SECRET_MIN_LENGTH): string {
    $secret_length = ($secret_length < self::SECRET_MIN_LENGTH) ? self::SECRET_MIN_LENGTH : $secret_length;
    $secret = Crypt::randomBytesBase64($secret_length);

    if ($save_secret) {
      $this->state->set(self::SECRET_KEY_NAME, $secret);
    }

    return $secret;
  }

  /**
   * Gets the current secret key from the State API.
   *
   * @return string|null
   */
  public function getSecretKey(): ?string {
    return $this->state->get(self::SECRET_KEY_NAME);
  }

  /**
   * Deletes the current key from the State API.
   *
   * @return void
   */
  public function deleteSecretKey(): void {
    $this->state->delete(self::SECRET_KEY_NAME);
  }

}