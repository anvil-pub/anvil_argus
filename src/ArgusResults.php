<?php

declare(strict_types=1);

namespace Drupal\anvil_argus;

use Drupal\Core\Datetime\DrupalDateTime;

class ArgusResults {

  /**
   * @var string
   */
  protected string $category;

  /**
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected DrupalDateTime $generated;

  /**
   * @var \Drupal\anvil_argus\ArgusResult[]
   */
  protected array $results = [];

  /**
   * @param string $category
   */
  public function __construct(string $category) {
    $this->category = $category;
    $this->generated = new DrupalDateTime();
  }

  /**
   * @param \Drupal\anvil_argus\ArgusResult $result
   *
   * @return $this
   */
  public function addResult(ArgusResult $result): self {
    $this->results[$result->getId()] = $result;

    return $this;
  }

  /**
   * @return ArgusResult[]
   */
  public function getResults(): array {
    return $this->results;
  }

  /**
   * @return string
   */
  public function toJson(): string {
    $data = [];

    foreach ($this->results as $result) {
      $data[$result->getSubcategory()][$result->getId(TRUE)] = $result->toArray();
    }

    return (string) json_encode([
      'timestamp' => $this->generated->getTimestamp(),
      'results' => $data,
    ]);
  }

}