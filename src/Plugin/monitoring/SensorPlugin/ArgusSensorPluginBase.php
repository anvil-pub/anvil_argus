<?php

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;

abstract class ArgusSensorPluginBase extends SensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultConfiguration(): array {
    return [
      'subcategory' => 'general',
    ] + parent::getDefaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['subcategory'] = [
      '#type' => 'select',
      '#options' => [
        'general' => $this->t('General'),
        'performance' => $this->t('Performance'),
        'security' => $this->t('Security'),
      ],
      '#title' => $this->t('Subcategory'),
      '#default_value' => $this->sensorConfig->getSetting('subcategory'),
      '#description' => $this->t('The subcategory under which this Sensor will be displayed on the Argus endpoint.'),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

}