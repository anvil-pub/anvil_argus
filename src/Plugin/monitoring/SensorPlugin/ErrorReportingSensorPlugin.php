<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Config\Config;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the error reporting level.
 *
 * @SensorPlugin(
 *   id = "argus_error_reporting",
 *   label = @Translation("Error reporting"),
 *   description = @Translation("Monitors the error reporting level."),
 *   addable = FALSE
 * )
 */
class ErrorReportingSensorPlugin extends ArgusSensorPluginBase {

  /**
   * Contains the system.logging configuration object.
   */
  protected Config $loggingConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, Config $logging_config) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->loggingConfig = $logging_config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('system.logging'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $error_reporting_level = $this->loggingConfig->get('error_level');

    $sensor_result->setValue($error_reporting_level);

    switch ($error_reporting_level) {
      case ERROR_REPORTING_DISPLAY_VERBOSE:
      case ERROR_REPORTING_DISPLAY_ALL:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_CRITICAL);
        $sensor_result->setMessage('The site is displaying all error messages');
        break;

      case ERROR_REPORTING_DISPLAY_SOME:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
        $sensor_result->setMessage('The site is displaying some error messages');
        break;

      case ERROR_REPORTING_HIDE:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
        $sensor_result->setMessage('The site is not displaying any error messages');
        break;
    }
  }

}