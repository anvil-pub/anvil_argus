<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\anvil_argus\Result\ArgusSensorResultInterface;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the current release of a specific theme.
 *
 * @SensorPlugin(
 *   id = "argus_theme_release",
 *   label = @Translation("Theme release"),
 *   description = @Translation("Monitors the theme's release."),
 *   addable = FALSE
 * )
 */
class ThemeReleaseSensorPlugin extends ExtensionReleaseSensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.theme'),
      $container->get('http_client'),
      $container->get('logger.channel.anvil_argus'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface|ArgusSensorResultInterface $sensor_result): void {
    if ($theme = $this->getSensorConfigSetting('theme')) {
      if ($this->extensionList->exists($theme)) {
        $extension = $this->extensionList->get($theme);

        $this->setSensorResultValues($sensor_result, $extension);
      }
    }
  }

}