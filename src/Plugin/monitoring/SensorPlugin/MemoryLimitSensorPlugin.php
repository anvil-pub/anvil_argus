<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\monitoring\Result\SensorResultInterface;

/**
 * Monitors the memory limit.
 *
 * @SensorPlugin(
 *   id = "argus_memory_limit",
 *   label = @Translation("Memory limit"),
 *   description = @Translation("Monitors the memory limit."),
 *   addable = FALSE
 * )
 */
class MemoryLimitSensorPlugin extends ArgusSensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $memory_limit = $this->getMemoryLimit();

    $sensor_result->setValue($memory_limit);
    $sensor_result->setMessage('@memory_limit MB', [
      '@memory_limit' => $memory_limit,
    ]);
  }

  /**
   * Returns the memory limit.
   *
   * @return int
   *   The memory limit in megabytes.
   */
  protected function getMemoryLimit(): int {
    $memory_limit = ini_get('memory_limit');

    // A value of -1 means no memory limit (e.g. unlimited).
    return ($memory_limit == -1) ? 0 : (int) preg_replace('/[^0-9.]+/', '', $memory_limit);
  }

}