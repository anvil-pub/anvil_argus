<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the site's page cache settings.
 *
 * @SensorPlugin(
 *   id = "argus_page_cache",
 *   label = @Translation("Page cache"),
 *   description = @Translation("Monitors the site's page cache settings."),
 *   addable = FALSE
 * )
 */
class PageCacheSensorPlugin extends ArgusSensorPluginBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    // ..
    $page_cache_default = $this->moduleHandler->moduleExists('page_cache');
    $page_cache_dynamic = $this->moduleHandler->moduleExists('dynamic_page_cache');

    if (!($page_cache_default && $page_cache_dynamic)) {
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_CRITICAL);

      if ($page_cache_default && !$page_cache_dynamic) {
        $sensor_result->setValue('partially enabled');
        $sensor_result->setMessage('Internal Dynamic Page Cache is disabled');
      }
      elseif(!$page_cache_default && $page_cache_dynamic) {
        $sensor_result->setValue('partially enabled');
        $sensor_result->setMessage('Internal Page Cache is disabled');
      }
      else {
        $sensor_result->setValue('disabled');
        $sensor_result->setMessage('Both Internal Page Cache and Internal Dynamic Page Cache are disabled');
      }
    }
    else {
      $sensor_result->setValue('enabled');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
      $sensor_result->setMessage('Both Internal Page Cache and Internal Dynamic Page Cache are enabled');
    }
  }

}