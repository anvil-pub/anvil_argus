<?php
declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the twig debugging settings.
 *
 * @SensorPlugin(
 *   id = "argus_twig_debugging",
 *   label = @Translation("Twig debugging"),
 *   description = @Translation("Monitors the Twig debugging settings."),
 *   addable = FALSE,
 * )
 */
class TwigDebuggingSensorPlugin extends ArgusSensorPluginBase {

  /**
   * Holds the container instance.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected ContainerInterface $container;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, ContainerInterface $container) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {

    $twig_config = $this->container->getParameter('twig.config');
    $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);

    if (!empty($twig_config['debug'])) {
      $sensor_result->setValue('partially enabled');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
      $sensor_result->addStatusMessage('Twig debug mode is enabled');
    }

    if (isset($twig_config['cache']) && !$twig_config['cache']) {
      $sensor_result->setValue('partially enabled');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
      $sensor_result->addStatusMessage('Twig cache disabled');
    }

    if (!empty($twig_config['auto_reload'])) {
      $sensor_result->setValue('partially enabled');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
      $sensor_result->addStatusMessage('Twig auto reload is enabled');
    }

    $sensor_status = $sensor_result->getStatus();
    if ($sensor_status == SensorResultDataInterface::STATUS_OK) {
      $sensor_result->setValue('disabled');
      $sensor_result->setMessage('Twig debugging is disabled');
    }
  }

}