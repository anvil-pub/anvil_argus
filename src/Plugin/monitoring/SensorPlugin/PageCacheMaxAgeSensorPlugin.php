<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Config\Config;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the site's page cache max age setting.
 *
 * @SensorPlugin(
 *   id = "argus_page_cache_max_age",
 *   label = @Translation("Page cache max age"),
 *   description = @Translation("Monitors the site's page cache max age setting."),
 *   addable = TRUE
 * )
 */
class PageCacheMaxAgeSensorPlugin extends PerformanceSensorPluginBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, Config $performance_config, DateFormatterInterface $date_formatter) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition, $performance_config);

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('system.performance'),
      $container->get('date.formatter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $page_cache_max_age = $this->performanceConfig->get('cache.page.max_age');

    $sensor_result->setValue($page_cache_max_age);

    if ($page_cache_max_age > 0) {
      $sensor_result->setMessage('@max_age_seconds seconds (or @max_age_formatted)', [
        '@max_age_seconds' => $page_cache_max_age,
        '@max_age_formatted' => $this->dateFormatter->formatInterval($page_cache_max_age),
      ]);
    }
    else {
      $sensor_result->setMessage('@max_age_seconds seconds', [
        '@max_age_seconds' => $page_cache_max_age,
      ]);
    }
  }

}