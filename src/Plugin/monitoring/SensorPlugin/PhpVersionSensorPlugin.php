<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;

/**
 * Monitors the PHP version.
 *
 * @SensorPlugin(
 *   id = "argus_php_version",
 *   label = @Translation("PHP version"),
 *   description = @Translation("Monitors the PHP version."),
 *   addable = FALSE
 * )
 */
class PhpVersionSensorPlugin extends ArgusSensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $php_version = $this->getPhpVersion();

    $sensor_result->setValue($php_version['version_exact']);

    if (version_compare($php_version['version_exact'], '8.2.0') >= 0) {
      $sensor_result->setMessage('PHP version is at least 8.2');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
    }
    elseif (version_compare($php_version['version_exact'], '8.0.0') >= 0) {
      $sensor_result->setMessage('PHP version is between 8.0 and 8.1');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
    }
    else {
      $sensor_result->setMessage('PHP version is below 8.0');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_CRITICAL);
    }
  }

  /**
   * Gets the current PHP major and exact versions.
   *
   * @return array
   */
  protected function getPhpVersion(): array {
    // Get the exact PHP version (e.g. 8.1.21).
    $version_exact = phpversion();

    // Get the major PHP version (e.g. 8).
    preg_match("#^\d#", $version_exact, $match);
    $version_major = $match[0];

    return [
      'version_exact' => $version_exact,
      'version_major' => $version_major,
    ];
  }

}