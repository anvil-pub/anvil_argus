<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Config\Config;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the site's mail address.
 *
 * @SensorPlugin(
 *   id = "argus_site_mail",
 *   label = @Translation("Site mail"),
 *   description = @Translation("Monitors the site's mail address."),
 *   addable = FALSE
 * )
 */
class SiteEmailSensorPlugin extends ArgusSensorPluginBase {

  const DISALLOWED_DOMAINS = [
    'anvil.be',
    'example.com',
    'ausy.be',
    'calibrate.be',
  ];

  /**
   * Contains the system.site configuration object.
   */
  protected Config $siteConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, Config $site_config) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->siteConfig = $site_config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('system.site'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $site_mail = $this->getSiteMail();

    $sensor_result->setValue($site_mail['mail_address']);

    if (in_array($site_mail['mail_domain'], self::DISALLOWED_DOMAINS)) {
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
      $sensor_result->setMessage('Site mail is possibly from a disallowed or incorrect domain');
    }
    else {
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
      $sensor_result->setMessage('Site mail is not from a disallowed or incorrect domain');
    }
  }

  /**
   * Gets the configured site mail.
   *
   * @return array
   */
  protected function getSiteMail(): array {
    $site_mail = $this->siteConfig->get('mail');

    return [
      'mail_address' => $site_mail,
      'mail_domain' => preg_replace('!^.+?([^@]+)$!', '$1', $site_mail),
    ];
  }


}