<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Database\Connection;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the admin username.
 *
 * @SensorPlugin(
 *   id = "argus_admin_username",
 *   label = @Translation("Admin username"),
 *   description = @Translation("Monitors the admins username."),
 *   addable = FALSE
 * )
 */
class AdminUsernameSensorPlugin extends ArgusSensorPluginBase {

  const DISALLOWED_USERNAMES = [
    'admin',
    'administrator',
    'user',
    'test',
    'root',
  ];

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $admin_username = $this->getAdminUsername();

    $sensor_result->setValue($admin_username);

    if (in_array($admin_username, self::DISALLOWED_USERNAMES)) {
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
      $sensor_result->setMessage('The administrator username is too obvious');
    }
    else {
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
      $sensor_result->setMessage('The administrator username is deemed safe');
    }
  }

  /**
   * Gets the username of the administrator account.
   *
   * @return string
   */
  protected function getAdminUsername(): string {
    $query = $this->database
      ->select('users_field_data', 'ufd')
      ->fields('ufd', ['name'])
      ->condition('uid', 1)
      ->execute();

    return $query->fetchField();
  }

}