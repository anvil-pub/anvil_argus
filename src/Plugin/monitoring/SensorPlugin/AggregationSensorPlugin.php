<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;

/**
 * Monitors the site's aggregation settings.
 *
 * @SensorPlugin(
 *   id = "argus_aggregation",
 *   label = @Translation("Aggregation"),
 *   description = @Translation("Monitors the site's aggregation settings."),
 *   addable = FALSE
 * )
 */
class AggregationSensorPlugin extends PerformanceSensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $preprocess_css = $this->performanceConfig->get('css.preprocess');
    $preprocess_js = $this->performanceConfig->get('js.preprocess');

    $preprocess = (bool) ($preprocess_css && $preprocess_js);

    if (!$preprocess) {
      if ($preprocess_css && !$preprocess_js) {
        $sensor_result->setValue('partially enabled');
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
        $sensor_result->setMessage('Only CSS aggregation is enabled');
      }
      elseif (!$preprocess_css && $preprocess_js) {
        $sensor_result->setValue('partially enabled');
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);
        $sensor_result->setMessage('Only JS aggregation is enabled');
      }
      else {
        $sensor_result->setValue('disabled');
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_CRITICAL);
        $sensor_result->setMessage('No aggregations are enabled');
      }
    }
    else {
      $sensor_result->setValue('enabled');
      $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
      $sensor_result->setMessage('All aggregations are enabled');
    }
  }

}