<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\anvil_argus\Result\ArgusSensorResultInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ExtensionVersion;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\TransferException;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;

abstract class ExtensionReleaseSensorPluginBase extends ArgusSensorPluginBase {

  const BASE_RELEASE_URL = 'https://updates.drupal.org/release-history/%s/current';

  const RELEASE_STATUS_UNKNOWN = 'unknown';
  const RELEASE_STATUS_LATEST = 'latest';
  const RELEASE_STATUS_OUTDATED = 'outdated';
  const RELEASE_STATUS_COMPROMISED = 'compromised';
  const RELEASE_STATUS_UNSUPPORTED = 'unsupported';

  /**
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected ExtensionList $extensionList;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, ExtensionList $extension_list, ClientInterface $http_client, LoggerInterface $logger) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->extensionList = $extension_list;
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    // ..
    $form['subcategory']['#options'] = [
      'modules' => $this->t('Modules'),
      'themes' => $this->t('Themes'),
    ];

    $extension_type = $this->sensorConfig->getSetting('module') ? 'module' : 'theme';

    // ..
    $form[$extension_type] = [
      '#type' => 'hidden',
      '#default_value' => $this->sensorConfig->getSetting($extension_type),
    ];

    return $form;
  }

  /**
   * Gets a setting from the Sensor configuration.
   *
   * @param string $key
   *   The key for which to get the Sensor configuration.
   *
   * @return mixed|null
   */
  public function getSensorConfigSetting(string $key): mixed {
    return $this->sensorConfig->getSetting($key);
  }

  /**
   * Sets the Sensor's result value, status and message.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface|\Drupal\anvil_argus\Result\ArgusSensorResultInterface $sensor_result
   *   The Sensor result for which to set the values.
   * @param \Drupal\Core\Extension\Extension $extension
   *   The extension the Sensor is monitoring.
   *
   * @return void
   */
  public function setSensorResultValues(SensorResultInterface|ArgusSensorResultInterface $sensor_result, Extension $extension): void {
    $sensor_result->setValue($extension->info['version']);

    $extension_update_status = $this->getExtensionUpdateStatus($extension);

    $meta_data = [
      'project_name' => $extension_update_status['info']['name'],
    ];

    // Check current release vs. potential other available releases, and
    // determine both the status and the message for the Sensor.
    switch ($extension_update_status['status']) {
      // The current release is considered 'comprimised', due to a security
      // being available for the current release.
      case self::RELEASE_STATUS_COMPROMISED:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_CRITICAL);

        if (isset($extension_update_status['recommended'])) {
          $sensor_result->setMessage('@existing is unsecure, upgrade to @latest', [
            '@existing' => $extension_update_status['existing_version'],
            '@latest' => $extension_update_status['recommended'],
          ]);

          $meta_data['recommended_release'] = $extension_update_status['recommended'];
        }
        else {
          $sensor_result->setMessage('@existing is unsecure, with no available alternatives', [
            '@existing' => $extension_update_status['existing_version'],
          ]);
        }
        break;

      // The current release is not supported anymore.
      case self::RELEASE_STATUS_UNSUPPORTED:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_WARNING);

        if (isset($extension_update_status['recommended'])) {
          $sensor_result->setMessage('@existing is unsupported, upgrade to @latest', [
            '@existing' => $extension_update_status['existing_version'],
            '@latest' => $extension_update_status['recommended'],
          ]);

          $meta_data['recommended_release'] = $extension_update_status['recommended'];
        }
        else {
          $sensor_result->setMessage('@existing is unsupported, with no available alternatives', [
            '@existing' => $extension_update_status['existing_version'],
          ]);
        }
        break;

      // The current release is outdated, as a new release is available.
      // However the current release is not at risk, so an update is not
      // actually required.
      case self::RELEASE_STATUS_OUTDATED:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_INFO);

        $meta_data['recommended_release'] = $extension_update_status['recommended'];

        // Check for possible alternative releases.
        if (isset($extension_update_status['also'])) {
          $sensor_result->setMessage('@latest available (also @alternatives available)', [
            '@latest' => $extension_update_status['recommended'],
            '@alternatives' => implode(', ', $extension_update_status['also']),
          ]);

          $meta_data['alternative_releases'] = array_values($extension_update_status['also']);
        }
        else {
          $sensor_result->setMessage('@latest available', [
            '@latest' => $extension_update_status['recommended'],
          ]);
        }
        break;

      // The current release is the latest release, so all good.
      case self::RELEASE_STATUS_LATEST:
        $sensor_result->setStatus(SensorResultDataInterface::STATUS_OK);
        $sensor_result->setMessage('Latest version');
        break;

      // The status of the current release cannot be determined. This could be
      // due to a self-hosted module, for example.
      case self::RELEASE_STATUS_UNKNOWN:
        $sensor_result->setMessage('@version (unknown release)', [
          '@version' => $extension->info['version'],
        ]);
        break;
    }

    $sensor_result->setMetaData($meta_data);
  }

  /**
   * Gets the update information of an extension, by first fetching then
   * comparing the current release information, with possible other releases.
   *
   * @param \Drupal\Core\Extension\Extension $extension
   *
   * @return array
   */
  protected function getExtensionUpdateStatus(Extension $extension): array {
    $extension_project_data = $this->prepareExtensionProjectData($extension);

    if ($extension_release_data = $this->getExtensionReleaseData($extension)) {
      $this->compileExtensionUpdateStatus($extension_project_data, $extension_release_data);
    }
    else {
      $extension_project_data['status'] = self::RELEASE_STATUS_UNKNOWN;
    }

    return $extension_project_data;
  }

  /**
   * Prepares the information of an extension for use in the comparison.
   *
   * @param \Drupal\Core\Extension\Extension $extension
   *
   * @return array
   */
  protected function prepareExtensionProjectData(Extension $extension): array {
    return [
      'name' => $extension->getName(),
      'info' => $extension->info,
      'datestamp' => $extension->info['mtime'],
      'project_type' => $extension->info['type'],
      'project_status' => true,
      'existing_version' => $extension->info['version'],
      'existing_major' => $extension->info['version'] ? ExtensionVersion::createFromVersionString($extension->info['version'])->getMajorVersion() : '',
      'install_type' => 'official',
    ];
  }

  /**
   * Fetches the project information for a given extension.
   *
   * @param \Drupal\Core\Extension\Extension $extension
   *
   * @return array|null
   */
  protected function getExtensionReleaseData(Extension $extension): ?array {
    $data = [];

    if ($xml = $this->fetchExtensionReleaseDataXml($extension)) {
      $data = $this->parseExtensionReleaseDataXml($xml);
    }

    return $data;
  }

  /**
   * Fetches the project information XML for a given extension.
   *
   * @param \Drupal\Core\Extension\Extension $extension
   *
   * @return string
   */
  protected function fetchExtensionReleaseDataXml(Extension $extension): string {
    $project_info_xml = '';

    // Immediately stop if there's no 'project' information available. The most
    // common cause would be for non-hosted modules (e.g. internal modules).
    if (!array_key_exists('project', $extension->info)) {
      return $project_info_xml;
    }

    // Build the URL to fetch the project information from.
    $extension_release_url = Url::fromUri(sprintf(self::BASE_RELEASE_URL, $extension->info['project']), [
      'query' => [
        'version' => $extension->info['version'],
        'list' => $extension->info['project'],
      ],
    ]);

    // Try and fetch the actual project information XML.
    try {
      $project_info_xml = (string) $this->httpClient
        ->get($extension_release_url->toString(), ['headers' => ['Accept' => 'text/xml']])
        ->getBody();
    }
    catch (TransferException|GuzzleException $exception) {
      $this->logger->warning('Unable to fetch release information for @project using @projecy_url: @error', [
        '@project' => $extension->info['project'],
        '@projecy_url' => $extension_release_url,
        '@error' => $exception->getMessage(),
      ]);
    }

    return $project_info_xml;
  }

  /**
   * Parses project information XML into an array.
   *
   * @param $raw_xml
   *
   * @return array|null
   */
  protected function parseExtensionReleaseDataXml($raw_xml): ?array {
    try {
      $xml = new SimpleXMLElement($raw_xml);
    }
    catch (\Exception $e) {
      // SimpleXMLElement::__construct produces an E_WARNING error message for
      // each error found in the XML data and throws an exception if errors
      // were detected. Catch any exception and return failure (NULL).
      return NULL;
    }

    // If there is no valid project data, the XML is invalid, so return failure.
    if (!isset($xml->short_name)) {
      return NULL;
    }

    $data = [];
    foreach ($xml as $k => $v) {
      $data[$k] = (string) $v;
    }

    $data['releases'] = [];

    if (isset($xml->releases)) {
      foreach ($xml->releases->children() as $release) {
        $version = (string) $release->version;
        $data['releases'][$version] = [];
        foreach ($release->children() as $k => $v) {
          $data['releases'][$version][$k] = (string) $v;
        }
        $data['releases'][$version]['terms'] = [];
        if ($release->terms) {
          foreach ($release->terms->children() as $term) {
            if (!isset($data['releases'][$version]['terms'][(string) $term->name])) {
              $data['releases'][$version]['terms'][(string) $term->name] = [];
            }
            $data['releases'][$version]['terms'][(string) $term->name][] = (string) $term->value;
          }
        }
      }
    }

    return $data;
  }

  /**
   * Compiles the update information for an extension, by comparing the current
   * release data, with the data fetched from Drupal.org.
   *
   * @param $extension_project_data
   * @param $extension_release_data
   *
   * @return void
   */
  protected function compileExtensionUpdateStatus(&$extension_project_data, $extension_release_data): void {
    if (isset($extension_release_data['project_status'])) {
      switch ($extension_release_data['project_status']) {
        case 'insecure':
          $extension_project_data['status'] = self::RELEASE_STATUS_COMPROMISED;
          break;

        case 'unpublished':
        case 'revoked':
        case 'unsupported':
          $extension_project_data['status'] = self::RELEASE_STATUS_UNSUPPORTED;
        break;
      }
    }

    if (!empty($extension_project_data['status'])) {
      // We already know the status for this project, so there's nothing else to
      // compute. Record the project status into $project_data and we're done.
      $extension_project_data['project_status'] = $extension_release_data['project_status'];
      return;
    }

    try {
      $existing_major = ExtensionVersion::createFromVersionString($extension_project_data['existing_version'])->getMajorVersion();
    }
    catch (\Exception $exception) {
      // If the version has an unexpected value we can't determine updates.
      $extension_project_data['status'] = self::RELEASE_STATUS_UNKNOWN;
      return;
    }

    $supported_branches = [];
    if (isset($extension_release_data['supported_branches'])) {
      $supported_branches = explode(',', $extension_release_data['supported_branches']);
    }

    $is_in_supported_branch = function ($version) use ($supported_branches) {
      foreach ($supported_branches as $supported_branch) {
        if (strpos($version, $supported_branch) === 0) {
          return TRUE;
        }
      }
      return FALSE;
    };

    if ($is_in_supported_branch($extension_project_data['existing_version'])) {
      // Still supported, stay at the current major version.
      $target_major = $existing_major;
    }
    elseif ($supported_branches) {
      // We know the current release is unsupported since it is not in
      // 'supported_branches' list. We should use the next valid supported
      // branch for the target major version.
      $extension_project_data['status'] = self::RELEASE_STATUS_UNSUPPORTED;
      foreach ($supported_branches as $supported_branch) {
        try {
          $target_major = ExtensionVersion::createFromSupportBranch($supported_branch)->getMajorVersion();
        }
        catch (\Exception $exception) {
          continue;
        }
      }
      if (!isset($target_major)) {
        // If there are no valid support branches, use the current major.
        $target_major = $existing_major;
      }

    }
    else {
      // Malformed XML file? Stick with the current branch.
      $target_major = $existing_major;
    }

    // Make sure we never tell the admin to downgrade. If we recommended an
    // earlier version than the one they're running, they'd face an
    // impossible data migration problem, since Drupal never supports a DB
    // downgrade path. In the unfortunate case that what they're running is
    // unsupported, and there's nothing newer for them to upgrade to, we
    // can't print out a "Recommended version", but just have to tell them
    // what they have is unsupported and let them figure it out.
    $target_major = max($existing_major, $target_major);

    // Defend ourselves from XML history files that contain no releases.
    if (empty($extension_release_data['releases'])) {
      $extension_project_data['status'] = self::RELEASE_STATUS_UNKNOWN;
      return;
    }

    $recommended_version_without_extra = '';
    $recommended_release = NULL;

    foreach ($extension_release_data['releases'] as $version => $release_info) {
      $release = [
        'published' => $release_info['status'] === 'published',
        'version' => $release_info['version'],
        'releaseUrl' => $release_info['release_link'],
        'releaseTypes' => $release_info['terms']['Release type'] ?? NULL,
        'coreCompatible' => $release_info['core_compatible'] ?? NULL,
        'coreCompatibilityMessage' => $release_info['core_compatibility_message'] ?? NULL,
        'downloadUrl' => $release_info['download_link'] ?? NULL,
        'date' => $release_info['date'] ?? NULL,
      ];

      try {
        $release_module_version = ExtensionVersion::createFromVersionString($release['version']);
      }
      catch (\Exception $exception) {
        continue;
      }
      // First, if this is the existing release, check a few conditions.
      if ($extension_project_data['existing_version'] === $version) {

        if ($release['releaseTypes'] && in_array('Insecure', $release['releaseTypes'], TRUE)) {
          $extension_project_data['status'] = self::RELEASE_STATUS_COMPROMISED;
        }
        elseif (!$release['published']) {
          $extension_project_data['status'] = self::RELEASE_STATUS_UNSUPPORTED;
          if (empty($extension_project_data['extra'])) {
            $extension_project_data['extra'] = [];
          }
          $extension_project_data['extra'][] = [
            'class' => ['release-revoked'],
            'label' => t('Release revoked'),
            'data' => t('Your currently installed release has been revoked, and is no longer available for download. Disabling everything included in this release or upgrading is strongly recommended!'),
          ];
        }
        elseif ($release['releaseTypes'] && in_array('Unsupported', $release['releaseTypes'], TRUE)) {
          $extension_project_data['status'] = self::RELEASE_STATUS_UNSUPPORTED;
          if (empty($extension_project_data['extra'])) {
            $extension_project_data['extra'] = [];
          }
          $extension_project_data['extra'][] = [
            'class' => ['release-not-supported'],
            'label' => t('Release not supported'),
            'data' => t('Your currently installed release is now unsupported, and is no longer available for download. Disabling everything included in this release or upgrading is strongly recommended!'),
          ];
        }
      }
      // Other than the currently installed release, ignore unpublished, insecure,
      // or unsupported updates.
      elseif (!$release['published'] ||
        !$is_in_supported_branch($release['version']) ||
        ($release['releaseTypes'] && in_array('Unsupported', $release['releaseTypes'], TRUE)) ||
        ($release['releaseTypes'] && in_array('Insecure', $release['releaseTypes'], TRUE))
      ) {
        continue;
      }

      $release_major_version = $release_module_version->getMajorVersion();
      // See if this is a higher major version than our target and yet still
      // supported. If so, record it as an "Also available" release.
      if ($release_major_version > $target_major) {
        if (!isset($extension_project_data['also'])) {
          $extension_project_data['also'] = [];
        }
        if (!isset($extension_project_data['also'][$release_major_version])) {
          $extension_project_data['also'][$release_major_version] = $version;
          $extension_project_data['releases'][$version] = $release_info;
        }
        // Otherwise, this release can't matter to us, since it's neither
        // from the release series we're currently using nor the recommended
        // release. We don't even care about security updates for this
        // branch, since if a project maintainer puts out a security release
        // at a higher major version and not at the lower major version,
        // they must remove the lower version from the supported major
        // versions at the same time, in which case we won't hit this code.
        continue;
      }

      // Look for the 'latest version' if we haven't found it yet. Latest is
      // defined as the most recent version for the target major version.
      if (!isset($extension_project_data['latest_version'])
        && $release_major_version == $target_major) {
        $extension_project_data['latest_version'] = $version;
        $extension_project_data['releases'][$version] = $release_info;
      }

      // Look for the development snapshot release for this branch.
      if (!isset($extension_project_data['dev_version'])
        && $release_major_version == $target_major
        && $release_module_version->getVersionExtra() === 'dev') {
        $extension_project_data['dev_version'] = $version;
        $extension_project_data['releases'][$version] = $release_info;
      }

      if ($release_module_version->getVersionExtra()) {
        $release_version_without_extra = str_replace('-' . $release_module_version->getVersionExtra(), '', $release['version']);
      }
      else {
        $release_version_without_extra = $release['version'];
      }

      // Look for the 'recommended' version if we haven't found it yet (see
      // phpdoc at the top of this function for the definition).
      if (!isset($extension_project_data['recommended'])
        && $release_major_version == $target_major) {
        if ($recommended_version_without_extra !== $release_version_without_extra) {
          $recommended_version_without_extra = $release_version_without_extra;
          $recommended_release = $release_info;
        }
        if ($release_module_version->getVersionExtra() === NULL) {
          $project_data['recommended'] = $recommended_release['version'];
          $project_data['releases'][$recommended_release['version']] = $recommended_release;
        }
      }

      // Stop searching once we hit the currently installed version.
      if ($extension_project_data['existing_version'] === $version) {
        break;
      }

      // If we're running a dev snapshot and have a timestamp, stop
      // searching for security updates once we hit an official release
      // older than what we've got. Allow 100 seconds of leeway to handle
      // differences between the datestamp in the .info.yml file and the
      // timestamp of the tarball itself (which are usually off by 1 or 2
      // seconds) so that we don't flag that as a new release.
      if ($extension_project_data['install_type'] == 'dev') {
        if (empty($extension_project_data['datestamp'])) {
          // We don't have current timestamp info, so we can't know.
          continue;
        }
        elseif ($release['date'] && $extension_project_data['datestamp'] + 100 > $release['date']) {
          // We're newer than this, so we can skip it.
          continue;
        }
      }

      if ($release['releaseTypes'] && in_array('Security update', $release['releaseTypes'], TRUE)) {
        $extension_project_data['security updates'][] = $release_info;
      }
    }

    // If we were unable to find a recommended version, then make the latest
    // version the recommended version if possible.
    if (!isset($extension_project_data['recommended']) && isset($extension_project_data['latest_version'])) {
      $extension_project_data['recommended'] = $extension_project_data['latest_version'];
    }

    if (isset($extension_project_data['status'])) {
      // If we already know the status, we're done.
      return;
    }

    // If we don't know what to recommend, there's nothing we can report.
    // Bail out early.
    if (!isset($extension_project_data['recommended'])) {
      $extension_project_data['status'] = self::RELEASE_STATUS_UNKNOWN;
      $extension_project_data['reason'] = t('No available releases found');
      return;
    }

    // If we're running a dev snapshot, compare the date of the dev snapshot
    // with the latest official version, and record the absolute latest in
    // 'latest_dev' so we can correctly decide if there's a newer release
    // than our current snapshot.
    if ($extension_project_data['install_type'] == 'dev') {
      if (isset($extension_project_data['dev_version']) && $extension_release_data['releases'][$extension_project_data['dev_version']]['date'] > $available['releases'][$extension_project_data['latest_version']]['date']) {
        $extension_project_data['latest_dev'] = $extension_project_data['dev_version'];
      }
      else {
        $extension_project_data['latest_dev'] = $extension_project_data['latest_version'];
      }
    }

    // Figure out the status, based on what we've seen and the install type.
    switch ($extension_project_data['install_type']) {
      case 'official':
        if ($extension_project_data['existing_version'] === $extension_project_data['recommended'] || $extension_project_data['existing_version'] === $extension_project_data['latest_version']) {
          $extension_project_data['status'] = self::RELEASE_STATUS_LATEST;
        }
        else {
          $extension_project_data['status'] = self::RELEASE_STATUS_OUTDATED;
        }
        break;

      case 'dev':
        $latest = $extension_release_data['releases'][$extension_project_data['latest_dev']];
        if (empty($extension_project_data['datestamp'])) {
          $extension_project_data['status'] = self::RELEASE_STATUS_UNKNOWN;
          $extension_project_data['reason'] = t('Unknown release date');
        }
        elseif (($extension_project_data['datestamp'] + 100 > $latest['date'])) {
          $extension_project_data['status'] = self::RELEASE_STATUS_LATEST;
        }
        else {
          $extension_project_data['status'] = self::RELEASE_STATUS_OUTDATED;
        }
        break;

      default:
        $extension_project_data['status'] = self::RELEASE_STATUS_UNKNOWN;
        $extension_project_data['reason'] = t('Invalid info');
    }
  }

}