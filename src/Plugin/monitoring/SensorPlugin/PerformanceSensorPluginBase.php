<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Config\Config;
use Drupal\monitoring\Entity\SensorConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class PerformanceSensorPluginBase extends ArgusSensorPluginBase {

  /**
   * Contains the system.performance configuration object.
   */
  protected Config $performanceConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(SensorConfig $sensor_config, $plugin_id, $plugin_definition, Config $performance_config) {
    parent::__construct($sensor_config, $plugin_id, $plugin_definition);

    $this->performanceConfig = $performance_config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('system.performance'),
    );
  }

}