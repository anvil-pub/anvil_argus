<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\anvil_argus\Result\ArgusSensorResultInterface;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors the current release of a specific module.
 *
 * @SensorPlugin(
 *   id = "argus_module_release",
 *   label = @Translation("Module release"),
 *   description = @Translation("Monitors the module's release."),
 *   addable = FALSE
 * )
 */
class ModuleReleaseSensorPlugin extends ExtensionReleaseSensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.module'),
      $container->get('http_client'),
      $container->get('logger.channel.anvil_argus'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface|ArgusSensorResultInterface $sensor_result): void {
    if ($module = $this->getSensorConfigSetting('module')) {
      if ($this->extensionList->exists($module)) {
        $extension = $this->extensionList->get($module);

        $this->setSensorResultValues($sensor_result, $extension);
      }
    }
  }

}