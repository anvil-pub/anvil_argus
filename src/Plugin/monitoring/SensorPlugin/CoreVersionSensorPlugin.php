<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Plugin\monitoring\SensorPlugin;

use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ExtensionVersion;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Result\SensorResultInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Monitors Drupal core version.
 *
 * @SensorPlugin(
 *   id = "argus_core_version",
 *   label = @Translation("Core version"),
 *   description = @Translation("Monitors the Drupal Core version."),
 *   addable = FALSE
 * )
 */
class CoreVersionSensorPlugin extends ExtensionReleaseSensorPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition) {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.module'),
      $container->get('http_client'),
      $container->get('logger.channel.anvil_argus'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runSensor(SensorResultInterface $sensor_result): void {
    $core_version = $this->getCoreVersion();

    // Modules and Themes are considered an Extension, and can be loaded
    // accordingly with the Extension List service.
    // Core however, is not an Extension that we can load so we fake it instead.
    // @TODO Fix and/or improve this.
    $extension = new Extension('', 'core', 'core/core.info.yml');
    $extension->info = [
      'project' => 'drupal',
      'version' => $core_version['version_exact'],
      'mtime' => 0,
      'type' => 'project_core',
      'name' => 'drupal',
    ];

    $this->setSensorResultValues($sensor_result, $extension);
  }

  /**
   * Gets the current Drupal Core major and exact versions.
   *
   * @return array
   */
  protected function getCoreVersion(): array {
    // Get the exact Drupal Core version (e.g. 10.2.7).
    $version_exact = \Drupal::VERSION;

    // Get the major Drupal Core version (e.g. 10).
    $version_major = ExtensionVersion::createFromVersionString($version_exact)->getMajorVersion();

    return [
      'version_exact' => $version_exact,
      'version_major' => $version_major,
    ];
  }

}