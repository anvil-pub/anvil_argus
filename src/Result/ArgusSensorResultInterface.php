<?php

namespace Drupal\anvil_argus\Result;

interface ArgusSensorResultInterface {

  /**
   * Sets the sensor metadata.
   *
   * @param array $data
   *
   * @return void
   */
  public function setMetaData(array $data): void;

  /**
   * Gets the sensor metadata.
   *
   * @return array
   */
  public function getMetaData(): array;

}