<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Result;

use Drupal\monitoring\Result\SensorResult;

class ArgusSensorResult extends SensorResult implements ArgusSensorResultInterface {

  /**
   * {@inheritdoc}
   */
  public function setMetaData(array $data): void {
    $this->setResultData('sensor_metadata', $data);
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaData(): array {
    return array_key_exists('sensor_metadata', $this->data) ? $this->getResultData('sensor_metadata') : [];
  }

}