<?php

declare(strict_types=1);

namespace Drupal\anvil_argus\Controller;

use Drupal\anvil_argus\ArgusDataGenerator;
use Drupal\anvil_argus\ArgusSecretManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for the Argus healthcheck endpoint.
 */
class ArgusIntegrationController extends ControllerBase {

  use LoggerChannelTrait;

  /**
   * @var \Drupal\anvil_argus\ArgusDataGenerator
   */
  protected ArgusDataGenerator $argusDataGenerator;

  /**
   * @var \Drupal\anvil_argus\ArgusSecretManager
   */
  protected ArgusSecretManager $argusSecretManager;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The core renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Timestamp when the results were last compiled.
   *
   * @var int
   */
  protected int $timestamp;

  /**
   * The maximum cache age.
   *
   * @var int
   */
  protected int $cacheMaxAge;

  /**
   * @param \Drupal\anvil_argus\ArgusDataGenerator $argus_data_generator
   * @param \Drupal\anvil_argus\ArgusSecretManager $argus_secret_manager
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(ArgusDataGenerator $argus_data_generator, ArgusSecretManager $argus_secret_manager, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack, RendererInterface $renderer) {
    $this->argusDataGenerator = $argus_data_generator;
    $this->argusSecretManager = $argus_secret_manager;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;

    $this->setLoggerFactory($logger_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('anvil_argus.generator'),
      $container->get('anvil_argus.secret_manager'),
      $container->get('logger.factory'),
      $container->get('request_stack'),
      $container->get('renderer'),
    );
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function buildJson(): JsonResponse {
    // Render response in context to avoid metadata leaks. Some monitoring
    // plugins are rendered on sensor run and cached at that time as well. That
    // metadata is hard to access, so we need rendering in context detour.
    $context = new RenderContext();
    $response = $this->renderer->executeInRenderContext($context, function () {
      if ($this->access($this->currentUser())->isForbidden()) {
        $json_response = new CacheableJsonResponse(
          '{"error": "Access denied!"}',
          403,
          [
            'Cache-Control' => 'private, no-cache, no-store, must-revalidate',
            'Last-Modified' => $this->formatDateString($this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME') ?? time()),
          ],
          TRUE
        );
      }
      else {
        $data = $this->argusDataGenerator->getData();

        $this->timestamp = json_decode($data, TRUE)['timestamp'] ?? '0';

        $json_response = new CacheableJsonResponse(
          $data,
          200,
          $this->buildHeaders(),
          TRUE
        );
      }
      // Cache is mostly set outside of context.
      $this->addCacheToResponse($json_response);
      return $json_response;
    });
    // If there is metadata left on the context, apply it on the response.
    if (!$context->isEmpty()) {
      if ($metadata = $context->pop()) {
        $this->refineCacheMetadata($metadata);
        $response->addCacheableDependency($metadata);
      }
    }
    return $response;
  }

  /**
   * Add cache information to the response.
   *
   * @param \Drupal\Core\Cache\CacheableResponseInterface $response
   *
   * @return \Drupal\Core\Cache\CacheableResponseInterface
   * @throws \Exception
   */
  protected function addCacheToResponse(CacheableResponseInterface $response): CacheableResponseInterface {
    if (!$response->headers->has('Cache-Control')) {
      $headers = $this->buildHeaders();
      foreach ($headers as $header => $value) {
        $response->headers->set($header, $value);
      }
    }

    $cacheable_metadata = $response->getCacheableMetadata();

    $this->refineCacheMetadata($cacheable_metadata);
    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

  /**
   * Adds cache metadata related to the healthheck.
   *
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $cacheable_dependency
   *   An object implementing RefinableCacheableDependencyInterface.
   */
  protected function refineCacheMetadata(RefinableCacheableDependencyInterface $cacheable_dependency): void {
    $cacheable_dependency->addCacheContexts([
      'headers:' . ArgusSecretManager::SECRET_KEY_NAME,
    ]);

    $cacheable_dependency->addCacheTags(['monitoring_sensor_result']);
    $cacheable_dependency->setCacheMaxAge($this->getCacheMaxAge() ?? 0);
  }

  /**
   * Access result callback.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Determines the access to controller.
   */
  public function access(AccountInterface $account): AccessResultInterface {
    $current_request = $this->requestStack->getCurrentRequest();

    // Fetch the secret the Argus server is passing on.
    $server_secret = $current_request->headers->get(ArgusSecretManager::SECRET_KEY_NAME) ?? $current_request->query->get(ArgusSecretManager::SECRET_KEY_NAME);

    // Fetch the secret the client has configured.
    $client_secret = $this->argusSecretManager->getSecretKey();

    if ($server_secret && $server_secret === $client_secret) {
      $access_result = AccessResult::allowed();
    }
    else {
      $access_result = AccessResult::forbidden();

      $this->getLogger('anvil_argus')->notice('Unauthorized access to monitoring results.');
    }

    $this->refineCacheMetadata($access_result);

    return $access_result;
  }

  /**
   * Return the header array.
   *
   * @return array
   *   Array of header names as keys and values as values.
   *
   * @throws \Exception
   */
  protected function buildHeaders(): array {
    $max_age = $this->getCacheMaxAge() ?? 0;
    $headers = [];
    if ($max_age) {
      $timestamp = $this->timestamp ?? 0;
      if (!$timestamp) {
        $this->getLogger('anvil_argus')->warning('The timestamp is missing.');
      }
      $expires = (int) $timestamp + (int) $max_age;
      $headers['Cache-Control'] = "public,max-age=$max_age,s-maxage=$max_age";
      $headers['Last-Modified'] = $this->formatDateString($timestamp);
      $headers['Expires'] = $this->formatDateString($expires);
      $headers['ETag'] = $timestamp;
    }
    else {
      $headers['Cache-Control'] = 'private,no-cache,must-revalidate';
      $headers['Last-Modified'] = $this->formatDateString($this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME') ?? time());
    }
    return $headers;
  }

  /**
   * @return int
   */
  protected function getCacheMaxAge(): int {
    if (!isset($this->cacheMaxAge)) {
      // @TODO Set default cache max age? Or let the Sensors handle it?
      $this->cacheMaxAge = 0;
    }

    return $this->cacheMaxAge;
  }

  /**
   * @param string $timestamp
   *
   * @return string
   * @throws \Exception
   */
  protected function formatDateString(int $timestamp): string {
    $date = new DrupalDateTime('@' . $timestamp);
    $date = $date->setTimezone(new \DateTimeZone('UTC'));
    return $date->format(\DATE_RFC2822, [
      'langcode' => 'en',
    ]);
  }

}