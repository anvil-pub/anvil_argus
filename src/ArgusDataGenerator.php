<?php

declare(strict_types=1);

namespace Drupal\anvil_argus;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\monitoring\SensorRunner;

class ArgusDataGenerator {

  use LoggerChannelTrait;

  /**
   * The sensor runner service.
   *
   * @var \Drupal\monitoring\SensorRunner
   */
  protected SensorRunner $sensorRunner;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $sensorConfigStorage;

  /**
   * @param \Drupal\monitoring\SensorRunner $sensor_runner
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(SensorRunner $sensor_runner, EntityTypeManagerInterface $entity_type_manager) {
    $this->sensorRunner = $sensor_runner;
    $this->sensorConfigStorage = $entity_type_manager->getStorage('monitoring_sensor_config');
  }

  /**
   * Returns all Sensor result data for a given category.
   *
   * @param string $category
   *   (optional) The category for which to return the Sensor results. Defaults
   *   to 'Argus'.
   *
   * @return string
   *   The Sensor results in JSON format.
   */
  public function getData(string $category = 'Argus'): string {
    // Get all enabled Sensor configurations belonging to the chosen category.
    /** @var \Drupal\monitoring\Entity\SensorConfig[] $sensors */
    $sensors = $this->sensorConfigStorage->loadByProperties([
      'category' => $category,
      'status' => TRUE,
    ]);

    $results = new ArgusResults($category);

    if ($sensors) {
      $sensor_results = $this->sensorRunner->runSensors($sensors);

      foreach ($sensor_results as $sensor_id => $sensor_result) {
        $results->addResult(new ArgusResult($sensor_id, $sensor_result));
      }
    }

    return $results->toJson();
  }

}